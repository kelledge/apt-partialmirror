from setuptools import setup, find_packages

import apt_partialmirror

package_details = {
  'name': 'apt-partialmirror',
  'version': apt_partialmirror.__version__,
  'author': 'Kevin Elledge',
  'author_email': 'kelldge@petropower.com',
  'url': 'https://bitbucket.org/kelledge/apt-partialmirror',

  'install_requires': ['docopt==0.6.1'],
  'tests_require': ['mock==1.0.1'],

  'packages': find_packages(),
  'package_data': {},
  'description': 'APT archive partial mirror administraion tool',

  'scripts': ['bin/apt-partialmirror']
}

setup(**package_details)