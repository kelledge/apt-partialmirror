"""
apt-partialmirror. Partial mirror administraion tool.

Usage:
  apt-partialmirror [options] include <package> ...
  apt-partialmirror (-h | --help)
  apt-partialmirror --version

Options:
  -a <archive>, --apt-archive <archive>   path to archive
  -d <dist>, --distribution <dist>        distribution to mirror
  -c <comp>, --component <comp>           comma delimited list of components
  -A <arch>, --architecture <arch>        comma delimited list of architectures
  -m <manifest>, --manifest <manifest>    path to package manifest file
  -h --help                               this message
  --version                               display version and exit
"""

from docopt import docopt
import apt
import uuid
import atexit
import shutil
import os
import re

import apt_partialmirror

def load_manifest_file(file_path):
  """
  Reads a dpkg selections file and returns 
  its list of pacakges as a set.
  """
  manifest_line = re.compile('([a-zA-Z0-9-.+]+)(:[a-zA-Z0-9]+)?\s+([a-zA-Z]+)')
  pkgs = set()
  # crate it if it does not exist
  if not os.path.exists(file_path):
    open(file_path, 'w+').close()    
  with open(file_path, 'r') as handle: 
    for line in handle:
      pkg_name, pkg_arch, pkg_state = manifest_line.match(line).groups()
      pkgs.add(pkg_name)
  return pkgs

def save_manifest_file(file_path, manifest):
  """
  Saves a set of pacakges as a dpkg selctions file.
  """
  with open(file_path, 'w+') as handle:
    for pkg in manifest:
      handle.write("%-30sinstall\n" % pkg)


def main():
  """
  Main entry point for apt-partialmirror.

  Curretly the product of several hours of crazed, coffee fueled, programming.
  But it works -- so its got that going for it ...
  """
  options = docopt(__doc__)
  if options['--version'] is not False:
    print "apt-partialmirror version %s" % apt_partialmirror.__version__
    return 0

  # use the first 8 characters of a UUID for a unique session name
  sess_id = str(uuid.uuid4())[:8] 
  manifest_file = os.path.abspath(options['--manifest'])
  manifest = load_manifest_file(manifest_file)

  rootfs_path = '/tmp/apt-partialmirror-%s/' % sess_id
  atexit.register(lambda: shutil.rmtree(rootfs_path)) # make sure session gets deleted

  # setup apt environment
  cache = apt.Cache(rootdir=rootfs_path)
  cache._check_and_create_required_dirs(rootfs_path)
  #
  architectures = options['--architecture'].split(',')
  components = options['--component'].split(',')
  source_entry = "deb {archive} {dist} {component}".format(
    archive=options['--apt-archive'],
    dist=options['--distribution'],
    component=' '.join(components)
  )
  # add each architecture
  for arch in architectures:
    apt.apt_pkg.config.set('APT::Architectures::', arch)  
  # inject source entry into sources.list
  with open(rootfs_path + 'etc/apt/sources.list', 'w+') as handle:
    handle.write(source_entry)


  # populate apt cache
  cache.open() # get sources.list into the cache before updating
  cache.update(fetch_progress=apt.progress.text.AcquireProgress())
  cache.open() # refresh our view of the cache to inlude the above update

  # marking a package as installed causes the cache to also
  # mark any dependent packages for installation. 
  print 'Resolving dependancies ...'
  try:
   for package in options['<package>']:
      cache[package].mark_install()
  except KeyError as e:
    print 'E: ' + e.message
    print 'I: Aborting ...'
    return 1


  pkg_list = set()
  for pkg in cache.get_changes():
    pkg_list.add(pkg.name)

  pkg_union = pkg_list | manifest
  new_pkgs = pkg_union - manifest
  pkg_list_sorted = sorted(pkg_union, key=lambda pkg: pkg)

  print 'I: Including %d new packages in %s' % (len(new_pkgs), manifest_file )
  for pkg in new_pkgs:
    print '  %s' % pkg

  save_manifest_file(manifest_file, pkg_list_sorted)
